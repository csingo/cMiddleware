package cMiddleware

import "github.com/gin-gonic/gin"

type MiddlewareInterface interface {
	MiddlewareName() (app, name string)
	Handler(ctx *gin.Context, params MiddlewareParams) (err error)
}
