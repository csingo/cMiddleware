package cMiddleware

import (
	"gitee.com/csingo/cComponents"
)

type MiddlewareComponent struct{}

func (i *MiddlewareComponent) Inject(instance any) bool {
	if container.Is(instance) {
		return container.Save(instance)
	}
	return false
}

func (i *MiddlewareComponent) InjectConf(config cComponents.ConfigInterface) bool {
	return false
}

func (i *MiddlewareComponent) Load() {}

func (i *MiddlewareComponent) Listen() []*cComponents.ConfigListener {
	return []*cComponents.ConfigListener{}
}

var Component = &MiddlewareComponent{}
