package cMiddleware

type MiddlewareParams map[string]any

func (i MiddlewareParams) Get(key string, defaultValue any) any {
	return get_middleware_params_value(i, key, defaultValue)
}

func (i MiddlewareParams) GetInt(key string, defaultValue int) int {
	return get_middleware_params_value(i, key, defaultValue)
}

func (i MiddlewareParams) GetInt64(key string, defaultValue int64) int64 {
	return get_middleware_params_value(i, key, defaultValue)
}

func (i MiddlewareParams) GetFloat64(key string, defaultValue float64) float64 {
	return get_middleware_params_value(i, key, defaultValue)
}

func (i MiddlewareParams) GetString(key string, defaultValue string) string {
	return get_middleware_params_value(i, key, defaultValue)
}

func (i MiddlewareParams) GetBool(key string, defaultValue bool) bool {
	return get_middleware_params_value(i, key, defaultValue)
}

func get_middleware_params_value[T any](params MiddlewareParams, key string, defaultValue T) T {
	if params == nil {
		return defaultValue
	}

	if v, ok := params[key]; ok {
		return v.(T)
	}

	return defaultValue
}
